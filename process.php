<?php
function get_youtube_title($video_id){
    $html = 'https://www.googleapis.com/youtube/v3/videos?id='.$video_id.'&key=AIzaSyCQ9C8NalClTHE2iqLl4-DBpKCiXEQ7pIg&part=snippet';
    $response = file_get_contents($html);
    $decoded = json_decode($response, true);
    foreach ($decoded['items'] as $items) {
         $title= $items['snippet']['title'];
         return $title;
    }
}
if(isset($_POST['yt_link'])&&!empty($_POST['yt_link'])) {
	$yt_id= explode("youtube.com/watch?v=", $_POST['yt_link']);
	//shell_exec("youtube-dl -x --audio-format mp3 " . $_POST['yt_link']);
	$title = get_youtube_title($yt_id[1]);
	$file = $title . "-" . $yt_id[1] . ".mp3";
	header("Location: " . $file);
}
else {
	header("Location: index.php?err");
}